
Retry Media Mover Module
========================
Module runs active media mover's configurations with files which processing was not correctly finished.

Module doesn't have any avaliable settings and run inside cron.

Install
-------
1) Copy the mm_retry folder to the modules folder in your installation.
2) Enable the module using Administer -> Modules (/admin/build/modules)
3) Start cron (http://drupal.org/cron)

Support
-------
http://drupal.org/project/issues/mm_retry